﻿using ConsoleTool.Controllers;
using ConsoleTool.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleTool
{
    sealed class Program : IToolsView
    {
        readonly IToolsController toolsController;

        Program() 
        {
            toolsController = new ToolsController(this);
        }
        
        void Run(string[] args)
        {
            toolsController.ApplyTool(args);
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.Run(args);
            Console.ReadLine();
        }
    }
}
