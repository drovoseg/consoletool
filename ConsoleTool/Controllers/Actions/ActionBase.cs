﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleTool.Controllers.Actions
{
    abstract class ActionBase : IAction
    {
        protected abstract string SearchPattern { get; }

        public void Do(DirectoryInfo rootDirectory, FileInfo resultsFile)
        {
            if (!rootDirectory.Exists) throw new FileNotFoundException("Root directory not found");

            Stream stream = new FileStream(resultsFile.FullName, FileMode.Create, FileAccess.Write);
            TextWriter textWriter = new StreamWriter(stream);

            Do(rootDirectory, rootDirectory, textWriter);

            textWriter.Flush();
            textWriter.Close();
            textWriter.Dispose();
        }

        private void Do(DirectoryInfo rootDirectory, DirectoryInfo curDirectory, TextWriter textWriter)
        {
            FileInfo[] files;

            if (SearchPattern == null) files = curDirectory.GetFiles();
            else files = curDirectory.GetFiles(SearchPattern);

            foreach (FileInfo file in files)
            {
                WriteFileName(rootDirectory, file, textWriter);
            }

            foreach (DirectoryInfo directory in curDirectory.GetDirectories())
            {
                Do(rootDirectory, directory, textWriter);
            }
        }

        protected abstract void WriteFileName(DirectoryInfo rootDirectory, FileInfo file, TextWriter textWriter);
    }
}
