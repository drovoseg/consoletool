﻿using System;

namespace ConsoleTool.Controllers.Actions
{
    static class ActionSimpleFactory
    {
        public static IAction CreateAction(string actionType)
        {
            switch (actionType)
            {
                case "all": return new AllAction();
                case "cpp": return new CppAction();
                case "reversed1": return new Reversed1Action();
                case "reversed2": return new Reversed2Action();
            }

            return null;
        }
    }
}
