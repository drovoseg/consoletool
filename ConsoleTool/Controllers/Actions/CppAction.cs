﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleTool.Controllers.Actions
{
    sealed class CppAction : ActionBase
    {
        protected override string SearchPattern
        {
            get { return "*.cpp"; }
        }

        protected override void WriteFileName(DirectoryInfo rootDirectory, FileInfo file, TextWriter textWriter)
        {
            string fileName = file.FullName;
            textWriter.WriteLine(fileName.Replace(rootDirectory.FullName, "") + " /");
        }
    }
}
