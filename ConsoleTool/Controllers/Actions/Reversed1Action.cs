﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleTool.Controllers.Actions
{
    sealed class Reversed1Action : ActionBase
    {
        private readonly char[] SEPARATORS = new char[] { Path.DirectorySeparatorChar };        
        
        protected override string SearchPattern
        {
            get { return null; }
        }

        protected override void WriteFileName(System.IO.DirectoryInfo rootDirectory, System.IO.FileInfo file, System.IO.TextWriter textWriter)
        {
            string fileName = file.FullName;

            fileName = fileName.Replace(rootDirectory.FullName, "");

            string[] parts = fileName.Split(SEPARATORS, StringSplitOptions.RemoveEmptyEntries);
            
            Array.Reverse(parts);

            textWriter.WriteLine(Path.Combine(parts));
        }
    }
}
