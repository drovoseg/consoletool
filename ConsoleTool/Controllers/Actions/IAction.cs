﻿using System;
using System.IO;

namespace ConsoleTool.Controllers.Actions
{
    interface IAction
    {
        void Do(DirectoryInfo rootDirectory, FileInfo resultsFile);
    }
}
