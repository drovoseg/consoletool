﻿using ConsoleTool.Controllers.Actions;
using ConsoleTool.Properties;
using ConsoleTool.Views;
using System;
using System.IO;

namespace ConsoleTool.Controllers
{
    sealed class ToolsController : IToolsController
    {
        private IToolsView toolsView;
        
        public ToolsController(IToolsView toolsView)
        {
            if (toolsView == null) throw new ArgumentNullException("toolsView");

            this.toolsView = toolsView;
        }

        public void ApplyTool(string rootDirName, string actionType, string resultsFileName = "results.txt")
        {
            if (string.IsNullOrWhiteSpace(actionType))
            {
                toolsView.ShowMessage(Resources.ActionCannotBeEmpty);
                return;
            }

            DirectoryInfo rootDirectory = CreateRootDirectory(rootDirName);
            FileInfo resultsFile = CreateResultFIle(resultsFileName);

            if (rootDirectory == null || resultsFile == null) return;

            if (!rootDirectory.Exists)
            {
                toolsView.ShowMessage(Resources.RootDirectoryNotExist);
                return;
            }

            IAction action = ActionSimpleFactory.CreateAction(actionType);

            if (action == null)
            {
                toolsView.ShowMessage(Resources.UnknownAction);
                return;
            }

            try
            {
                action.Do(rootDirectory, resultsFile);
            } 
            catch (UnauthorizedAccessException)
            {
                toolsView.ShowMessage(Resources.AccessToResultDenied);
                return;
            }
            toolsView.ShowMessage(Resources.Success);
        }

        private FileInfo CreateResultFIle(string resultsFileName)
        {
            try
            {
                return new FileInfo(resultsFileName.Trim());
            }
            catch (ArgumentException)
            {
                toolsView.ShowMessage(Resources.ResultFilenameInvalidChars);
                return null;
            }
            catch (PathTooLongException)
            {
                toolsView.ShowMessage(Resources.ResultFilenameTooLong);
                return null;
            }
            catch (NotSupportedException)
            {
                toolsView.ShowMessage(Resources.ResultFilenameInvalidChars);
                return null;
            }
        }

        private DirectoryInfo CreateRootDirectory(string rootDirName)
        {
            try
            {
                return new DirectoryInfo(rootDirName.Trim());
            }
            catch (ArgumentException)
            {
                toolsView.ShowMessage(Resources.RootDirectoryInvalidChars);
                return null;
            }
            catch (PathTooLongException)
            {
                toolsView.ShowMessage(Resources.RootDirectoryNameTooLong);
                return null;
            }
        }

        public void ApplyTool(string[] args)
        {
            if (args.Length < 2)
            {
                toolsView.ShowMessage(Resources.InsufficientAmountArguments);
                return;
            }

            if (args.Length > 2)
            {
                ApplyTool(args[0], args[1], args[2]);
            }
            else ApplyTool(args[0], args[1]);
        }
    }
}
