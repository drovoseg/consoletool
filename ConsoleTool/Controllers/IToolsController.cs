﻿using System;

namespace ConsoleTool.Controllers
{
    interface IToolsController
    {
        void ApplyTool(string[] args);
        void ApplyTool(string rootDirName, string action, string resultsFileName);
    }
}
